# Exercice 01

L’idée de ce test est d’évaluer votre savoir-faire afin de mieux cerner votre profil et vos compétences.

Vous trouverez [ici la maquette](mockup.pdf) à intégrer. Cette maquette vous est fournie au format [PDF](mockup.pdf) et au format [XD](mockup.xd). Faites-en bonne usage.

Concernant le test en lui-même, n’oubliez pas qu’il s’agit d’une « mise en situation ». Réalisez ce test comme si vous étiez en poste, à travailler en équipe. Des questions, un blocage ou autre ? Aucun problème, cela nous arrive à tous, ne soyez pas timide !

Voici les seules contraintes que nous imposons à ce test :

- **La transparence** : il n’y a pas de piège, de contrainte forte ou quoi que ce soit d’autre. Nous vous demandons juste d’être transparent dans votre façon de réaliser ce test. Que cela soit sur le temps passé, les difficultés rencontrées, l’aide demandée, les outils utilisés..., tenez nous au courant !
- **JSON Server** : l’utilisation de [JSON Server](https://github.com/typicode/json-server) est un prérequis à ce test dans le but de dynamiser la partie « Présentation des personnages » à  minima. Dynamiser la partie « Actualités » serait également apprécié.

Vous êtes libre d’utiliser les outils que vous souhaitez pour la réalisation (IDE, technos …) comme pour nous rendre votre travail (zip, repo git …).

N’oubliez pas de joindre une rapide explication afin que l’on puisse regarder votre travail le plus efficacement possible. Chez nous, nous ouvrons, nous cliquons, nous testons. Rien de plus, rien de moins.

Pour le reste, libre à vous de faire comme bon vous semble. Alors à vous de jouer.

## Que la force soit avec vous !

Pour info, chez Useweb, notre stack technique se compose souvent de Vue.js et Tailwind.

Nous recommandons l’utilisation de Tailwind.
